# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/trusty64"

  config.vm.box_check_update = true

  config.vm.network "forwarded_port", guest: 8080, host: 8080

  config.vm.synced_folder ".", "/home/vagrant/source/"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "1024"
  end

  $SERVER_SETUP = <<-SCRIPT
    export DEBIAN_FRONTEND=noninteractive
    apt-get update
    apt-get install -y git
    apt-get install -y build-essential
    apt-get install -y libreadline6-dev
    apt-get install -y libyaml-dev
    apt-get install -y libsqlite3-dev
    apt-get install -y sqlite3
    apt-get install -y autoconf
    apt-get install -y libgdbm-dev
    apt-get install -y libncurses5-dev
    apt-get install -y automake
    apt-get install -y libtool
    apt-get install -y bison
    apt-get install -y libffi-dev
    apt-get install -y postgresql-client
    apt-get install -y postgresql-contrib
    apt-get install -y virtualenvwrapper
    apt-get build-dep -y psycopg2
    apt-get build-dep -y pillow
    apt-get build-dep -y python-lxml
    apt-get install -y openjdk-7-jre-headless
    apt-get install -y wget
    wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -
    echo "deb http://packages.elastic.co/elasticsearch/1.7/debian stable main" | tee -a /etc/apt/sources.list.d/elasticsearch-1.7.list
    apt-get update
    apt-get install -y elasticsearch
    update-rc.d elasticsearch defaults 95 10
    service elasticsearch start
  SCRIPT

  $VIRTUALENV_SETUP = <<-SCRIPT
    virtualenv ~/virtualenv
    source ~/virtualenv/bin/activate
    pip install -r /home/vagrant/source/requirements.txt
  SCRIPT

  config.vm.provision "shell", inline: $SERVER_SETUP, privileged: true
  config.vm.provision "shell", inline: $VIRTUALENV_SETUP, privileged: false

end
